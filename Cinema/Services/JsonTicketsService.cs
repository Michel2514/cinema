﻿using Cinema.Interfaces;
using Cinema.Models;
using Cinema.Models.Domain;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Web;

namespace Cinema.Services
{
    public class JsonTicketsService : ITicketsService
    {
        private const string PathToJson = "/Files/Data.json";
        private HttpContext Context { get; set; }

        public JsonTicketsService(HttpContext context)
        {
            Context = context;
        }

        public Movie GetMovieById(int id)
        {
            var fullModel = GetDataFromFile();
            return fullModel.Movies.FirstOrDefault(x=>x.Id == id);

        }

        public Movie[] GetAllMovies()
        {
            var fullModel = GetDataFromFile();
            return fullModel.Movies;
        }

        public Hall GetHallById(int id)
        {
            var fullModel = GetDataFromFile();
            return fullModel.Halls.FirstOrDefault(x => x.Id == id);
        }

        public TimeSlot[] GeTimeSlotsByMovieId(int movieId)
        {
            var fullModel = GetDataFromFile();
           return fullModel.TimeSlots.Where(x => x.MovieId == movieId).ToArray();
        }

        public Hall[] GetAllHall()
        {
            var fullModel = GetDataFromFile();
            return fullModel.Halls;
        }

        public TimeSlot GetTimesSlotById(int id)
        {
            var fullModel = GetDataFromFile();
            return fullModel.TimeSlots.FirstOrDefault(x => x.Id == id);
        }

        
        public TimeSlot[] GetAllTimeSlots()
        {
            var fullModel = GetDataFromFile();
            return fullModel.TimeSlots;
        }

        public bool UpdateMovie(Movie updateMovie)
        {
            var fullModel = GetDataFromFile();
            var movieToUpdate = fullModel.Movies.FirstOrDefault(x => x.Id == updateMovie.Id);
            if (movieToUpdate == null)
            {
                return false;
            }

            movieToUpdate.Title = updateMovie.Title;
            movieToUpdate.MinAge = updateMovie.MinAge;
            movieToUpdate.Director = updateMovie.Director;
            movieToUpdate.Description = updateMovie.Description;
            movieToUpdate.ImgUrl = updateMovie.ImgUrl;
            movieToUpdate.Rating = updateMovie.Rating;
            movieToUpdate.ReleseDate = updateMovie.ReleseDate;

            if (movieToUpdate.Genres != null)
            {
                movieToUpdate.Genres = updateMovie.Genres;
            }

            SaveToFile(fullModel);
            return true;
        }

        public bool UpdateHall(Hall updateHall)
        {
            var fullModel = GetDataFromFile();
            var hallToUpdate = fullModel.Halls.FirstOrDefault(x => x.Id == updateHall.Id);
            if (hallToUpdate == null)
                return false;

            hallToUpdate.Name = updateHall.Name;
            hallToUpdate.Places = updateHall.Places;

            SaveToFile(fullModel);
            return true;
        }

        public bool UpdateTimeslot(TimeSlot updeteTimeSlot)
        {
            var fullModel = GetDataFromFile();
            var timeslotToUpdate = fullModel.TimeSlots.FirstOrDefault(x => x.Id == updeteTimeSlot.Id);
            if (timeslotToUpdate == null)
                return false;

            timeslotToUpdate.Cost = updeteTimeSlot.Cost;
            timeslotToUpdate.Format = updeteTimeSlot.Format;
            timeslotToUpdate.HallId = updeteTimeSlot.HallId;
            timeslotToUpdate.MovieId = updeteTimeSlot.MovieId;
            timeslotToUpdate.StartTime = updeteTimeSlot.StartTime;

            SaveToFile(fullModel);
            return true;
        }

        private void SaveToFile(FileModel model)
        {
            var jsonFilePath = Context.Server.MapPath(PathToJson);
            var serializedModel = JsonConvert.SerializeObject(model);

            System.IO.File.WriteAllText(jsonFilePath,serializedModel);
        }

        private FileModel GetDataFromFile()
        {
            var jsonFilePath = Context.Server.MapPath(PathToJson);
            if (!System.IO.File.Exists(jsonFilePath))
                return null;

            var json = System.IO.File.ReadAllText(jsonFilePath);
            var fileModel = JsonConvert.DeserializeObject<FileModel>(json);
            return fileModel;
        }
    }
}