﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cinema.Models.Domain;

namespace Cinema.Interfaces
{
    interface ITicketsService
    {
        Movie GetMovieById(int id);
        Movie[] GetAllMovies();

        Hall GetHallById(int id);
        Hall[] GetAllHall();

        TimeSlot GetTimesSlotById(int id);
        TimeSlot[] GeTimeSlotsByMovieId(int movieId);
        TimeSlot[] GetAllTimeSlots();

        bool UpdateMovie(Movie updateMovie);
        bool UpdateHall(Hall updateHall);
        bool UpdateTimeslot(TimeSlot updeteTimeSlot);

    }
}
