﻿using System.Web.Mvc;
using Cinema.Interfaces;
using Cinema.Models.Domain;
using Cinema.Services;
using Newtonsoft.Json;

namespace Cinema.Controllers
{
    public class AdminController : Controller
    {
        private readonly ITicketsService _ticketsService;

        public AdminController()
        {
            _ticketsService = new JsonTicketsService(System.Web.HttpContext.Current);
        }

        public ActionResult FindMoviById(int id)
        {
            var movie = _ticketsService.GetMovieById(id);
            if (movie == null)
                return Content("Movie with such ID does not exist", "application/json");

            var movieJson = JsonConvert.SerializeObject(movie);
            return Content(movieJson, "application/json");
        }

        public ActionResult FindTimeslotById(int id)
        {
            var timeslot  = _ticketsService.GetTimesSlotById(id);
            if (timeslot == null)
                return Content("TimeSlot with such ID does not exist", "application/json");

            var timeslotJson = JsonConvert.SerializeObject(timeslot);
            return Content(timeslotJson, "application/json");
        }

        public ActionResult FindHallById(int id)
        {
            var hall = _ticketsService.GetHallById(id);
            if (hall == null)
                return Content("Hall with such ID does not exist", "application/json");

            var hallJson = JsonConvert.SerializeObject(hall);
            return Content(hallJson, "application/json");
        }

        public ActionResult MoviesList()
        {
            var movies = _ticketsService.GetAllMovies();
            return View("MoviesList", movies);
        }

        public ActionResult GetHallList()
        {
            var halls = _ticketsService.GetAllHall();
            return View("GetHallList", halls);
        }

        public ActionResult GetTimeslotsList()
        {
            var timeslots = _ticketsService.GetAllTimeSlots();
            return View("GetTimeslotsList", timeslots);
        }

        public ActionResult GetMovieTimeslotsList(int movieId)
        {
            var timeslotsMovies = _ticketsService.GeTimeSlotsByMovieId(movieId);
            return View("GetTimeslotsList", timeslotsMovies);
        }

        [HttpGet]
        public ActionResult EditMovie(int movieId)
        {
            var movie = _ticketsService.GetMovieById(movieId);
            return View("EditMovie", movie);
        }

        [HttpPost]
        public ActionResult EditMovie(Movie model)
        {
            if (ModelState.IsValid)
            {
                var updateResult = _ticketsService.UpdateMovie(model);
                if (updateResult)
                    return RedirectToAction("MoviesList");

                return Content("Update failed.");
            }
            return View("EditMovie", model);
        }

        [HttpGet]
        public ActionResult EditHall(int hallId)
        {
            var hall = _ticketsService.GetHallById(hallId);
            return View("EditHall", hall);
        }

        [HttpPost]
        public ActionResult EditHall(Hall hall)
        {
            if (ModelState.IsValid)
            {
                var updateResult = _ticketsService.UpdateHall(hall);
                if (updateResult)
                    return RedirectToAction("GetHallList");

                return Content("Update failed");
            }

            return View("EditHall", hall);
        }

        [HttpGet]
        public ActionResult EditTimeslot(int timeslotId)
        {
            var timeslot = _ticketsService.GetTimesSlotById(timeslotId);
            return View("EditTimeslot", timeslot);
        }

        [HttpPost]
        public ActionResult EditTimeslot(TimeSlot timeslot)
        {
            if (ModelState.IsValid)
            {
                var updateResult = _ticketsService.UpdateTimeslot(timeslot);
                if (updateResult)
                    return RedirectToAction("GetTimeslotsList");

                return Content("Update failed");
            }

            return View("EditTimeslot", timeslot);
        }
    }
}